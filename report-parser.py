"""
TRADE PARSER
Maintainer: Arthur Rahmangulov
"""
import csv
import yaml
import datetime
import urllib.request
import decimal
import xmltodict


# Write array to file
# Use in functions: aggregate_trades(), correct_vol_for_curr_fut() and create_pnl_report()
def write_array_to_file(array, filename, mode='write', each_line=1):
    if mode == 'append':
        file = open(filename, 'a')
    else:
        file = open(filename, 'w')

    if each_line == 1:
        for line in array:
            for each in line:
                file.write('%s\n' % str(each)[1:-1].replace("'", ""))
        file.close()
    elif each_line == 0:
        for line in array:
            file.write('%s' % line)
        file.close()


# Convert from csv to array
def create_array_from_csv(csv_filename):
    array = []      # Create array with trades for parse
    with open(csv_filename, newline='') as csv_file:
        read_report = csv.reader(csv_file, delimiter=',', quotechar='|')
        for row in read_report:
            array.append(row)
    return array


# Aggregate trades in arrays-rows at each line in result-file
def aggregate_trades(array):
    # Main array, contains all chains of trades
    chain_trades = []
    # Check the array if it has any lines except 0 or 1
    while len(array) > 1:
        # Define initial trade (contract, quantity, trade way), which help us to find similar trades
        initial_trade = array[0]
        queue_of_trades = [initial_trade, ]
        array.remove(initial_trade)
        trade_way = queue_of_trades[0][3]   # The 'trade_way' is direction of trade
        contract = queue_of_trades[0][2]
        counter = int(queue_of_trades[0][4])

        # Try to find similar trades with same contract as have a initial trade
        for detected in array[:]:
            if contract == detected[2]:
                queue_of_trades.append(detected)
                array.remove(detected)
                if not (trade_way == detected[3]):
                    counter -= int(queue_of_trades[-1][4])
                    if counter >= 0:
                        # If all similar trades were found, write result to result-file
                        if counter == 0:
                            queue_of_trades.append(trade_way)
                            chain_trades.append(queue_of_trades)
                            break   # And goes to define new initial trade
                    else:
                        trade_way = queue_of_trades[-1][3]  # Needs if trade changes the self way
                else:
                    counter += int(queue_of_trades[-1][4])
    else:
        # Send message if the task is complete!
        print('Task is complete!')
        return chain_trades


def get_specs_from_yaml(specs_filename):
    yaml_file = open(specs_filename)
    data = yaml.safe_load(yaml_file)
    yaml_file.close()
    return data


# If your report have trades with currency futures, for example as ED (EUR/USD) or GOLD
# you can edit file with price-step values. This value locate at
# http://moex.com/en/contract.aspx?code=<CONTRACT-NAME>-<MONTH>.<YEAR>
# for example: http://moex.com/en/contract.aspx?code=ED-12.13
# And you should be edit Value of price tick, it can be find in specifications from contracts.
# Locate at: http://moex.com/s255
def correct_vol_for_curr_fut(src_array, chains_array, specs_filename='specifications.yaml'):
    # Parse specification for currencies futures
    price_step_data = get_specs_from_yaml(specs_filename)['price tick']

    def get_exchange_rate(moment_start, moment_end):
        # Date should be in format yyyy-mm-dd
        url = urllib.request.urlopen('http://moex.com/export/derivatives/currency-rate.aspx?language=en&currency='
                                     'USD/RUB&moment_start={0}&moment_end={1}'.format(moment_start, moment_end))
        data_by_url = url.read()
        url.close()

        exchange_rate_by_url = xmltodict.parse(data_by_url, dict_constructor=dict)
        return exchange_rate_by_url

    def select_date_by_time(date, time):
        # Set date if time > 18:45 (evening session), go to next day
        src_date = datetime.datetime.strptime(date, '%Y-%m-%d')
        date = src_date.strftime('%Y-%m-%d')
        if time > '18:45:00':  # Evening clearing session (main clearing)
            date = (src_date + datetime.timedelta(days=1)).strftime('%Y-%m-%d')
            # If date is Saturday, set Monday
            if datetime.datetime.strptime(date, '%Y-%m-%d').isoweekday() == 5:
                date = (src_date + datetime.timedelta(days=3)).strftime('%Y-%m-%d')
        return date

    def search_exchange_rate_by_date(date):
        exchange_rate_by_date = next(rates['@value'] for rates in exchange_rate_data['rtsdata']['rates']['rate']
                                     if rates['@moment'] == '{0} {1}'.format(date, '18:30:00'))
        return exchange_rate_by_date

    def search_moment(reverse=0):
        if reverse == 0:
            moment = next(obj for obj in src_array if obj[2][:2] in price_step_data)
        else:
            moment = next(obj for obj in reversed(src_array) if obj[2][:2] in price_step_data)
        moment_date = datetime.datetime.strptime(moment[0], '%d.%m.%Y').strftime('%Y-%m-%d')
        moment_time = moment[1]
        moment = select_date_by_time(moment_date, moment_time)
        return moment

    # Search start and end moments if have trades w/ currency futures
    try:
        moment_start_date = search_moment(0)
        moment_end_date = search_moment(1)
        exchange_rate_data = get_exchange_rate(moment_start_date, moment_end_date)
    except StopIteration:
        print("Your report has no trades with currency futures")
        return 0

    # Correct volume
    for row in chains_array:
        contract_name = row[0][2][:2]
        if contract_name in price_step_data:
            price_step_value = price_step_data[contract_name]['price step']
            price_step_cost_usd = price_step_data[contract_name]['value of price tick in usd']

            for each in row[:-1]:
                # Set date by time; each[0] is date and each[1] is time from trade
                date_of_trade = select_date_by_time(datetime.datetime.strptime(each[0], '%d.%m.%Y')
                                                    .strftime('%Y-%m-%d'), each[1])

                # Search exchange rate by date;
                exchange_rate = search_exchange_rate_by_date(date_of_trade)

                # Calculate correct volume
                quantity = float(each[4])
                price_of_trade = float(each[5])
                correct_vol = decimal.Decimal(price_of_trade * float(exchange_rate) * price_step_cost_usd /
                                              price_step_value * quantity).quantize(decimal.Decimal('0.01'))
                each[6] = float(correct_vol)   # Replace old volume for correct volume

    print('Your result chain of trades now is correctly!')
    return chains_array


def create_pnl_report(array, specs_filename='specifications.yaml', output_filename='report_pnl.txt'):
    # Get commission values
    fees_data = get_specs_from_yaml(specs_filename)['fee']

    def edit_subtrades(subtrade):
        subtrade.insert(0, '{0}.{1}'.format(counter, sub_counter))   # Append SubTrade ID
        subtrade.insert(7, '')
        subtrade.insert(8, '')
        subtrade.insert(9, '')
        subtrade.insert(12, reg_comm * float(subtrade[5]))  # Append Trade Execution fees
        subtrade.insert(13, '')
        subtrade.insert(14, '')
        return subtrade

    def edit_maintrade(maintrade):
        maintrade.insert(0, [counter, maintrade[0][1], maintrade[0][2], contract_name, maintrade[-1], int(quantity / 2),
                             maintrade[0][6], maintrade[-2][6], maintrade[-2][1], maintrade[-2][2],
                             round(volume_of_trade, 2), clearing_comm, quantity * reg_comm, round(trade_pnl, 2)])
        maintrade.pop(-1)
        return maintrade

    counter = 0
    for row in array:
        contract_name = row[0][2]

        # Try to get commission value by contract name
        try:
            reg_comm = fees_data[contract_name[:2]]
        except TypeError:
            print("Your specification-file not contain same contract specified in report")
            write_array_to_file(array, output_filename)
            return 0

        sub_counter = 0
        quantity = 0
        clearing_comm = 0
        volume_for_pnl = 0
        volume_of_trade = 0
        counter += 1
        for each in row[:-1]:
            quantity += int(each[4])
            clearing_comm += float(each[7])
            volume_of_trade += float(each[6])
            if each[3] == 'Sell':
                volume_for_pnl += float(each[6])
            elif each[3] == 'Buy':
                volume_for_pnl -= float(each[6])
            else:
                print('Your trade has not contain right trade way')
            sub_counter += 1
            # Implement blank fields and other things
            edit_subtrades(each)
        result_comm = quantity * reg_comm + clearing_comm
        trade_pnl = volume_for_pnl - result_comm

        # Edit Main Trade
        edit_maintrade(row)

    write_array_to_file(array, output_filename)
    print('Excellent! You can check your P&L result in')


def main(input_filename='test_reports/example.csv', specs='specifications.yaml', output_filename='report_pnl.csv'):
    array = create_array_from_csv(input_filename)
    src_array = list(array)     # list like const source array
    chains_array = aggregate_trades(array)
    correct_chains_array = correct_vol_for_curr_fut(src_array, chains_array, specs)
    create_pnl_report(correct_chains_array, specs, output_filename)

main()
