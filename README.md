report-parser.py
================

This is my first-project which parse source report (see Example of source report) from Moscow Exchange.  
1. Found similar trades and concatenation their.  
2. Check if source repport have trades with currency futures and correct volume values (using the indicative rates).  
3. Calculate P&L ...  
4. Create output-file named of "report_pnl.csv" *(see Example of output P&L report)*

Example of source report from your broker
-----------------------------------------
| Date of trade | Time of trade | Contact name            | Direction of trade | Quantity      | Price of trade | Volume                                                                       | Registration fees       |
|---------------|---------------|-------------------------|--------------------|---------------|----------------|------------------------------------------------------------------------------|-------------------------|
| dd.mm.yyyy    | hh:mm:ss      | Contract Trading Symbol | Buy or Sell        | Integer value | Float or int   | Volume will be correcting if source report have trades with currency futures | Fees by Moscow Exchange |

Example of output P&L report (grouping, theme, header were made by hand)
------------------------------------------------------------------------
![Capture.PNG](https://bitbucket.org/repo/peB75o/images/1169857295-Capture.PNG)

TODO 1. Do automize grouping and add header to table. (help lib: [XslxWriter(Working with Outlines and Grouping)](http://xlsxwriter.readthedocs.io/working_with_outlines.html)
-------------------

TODO 2. Optimize algorithms
-------------------

*test-reports folder contains example source report from your broker.*  
*Of course I'll up my skill and improve program functional skill.*